# MyStory

#### 项目介绍

 **个人博客开源项目** 

#### 软件架构

软件架构说明：
- 核心框架：`SpringBoot2.0`
- 安全框架：`Apache Shiro 1.3.2`
- 缓存框架：`Redis 4.0`
- 任务调度：`quartz 2.3`
- 持久层框架：`MyBatis 3`
- 数据库连接池：`Alibaba Druid 1.0.2`
- 日志管理：`SLF4J 1.7、Log4j`
- 前端样式：`Tale`
- 上传框架：`DropZoneJs`

#### 安装教程
1. 需要jdk1.8+
2. maven环境，熟悉springboot
3. down代码到本地
4. 数据库文件放在sql中,直接导入到数据库中即可,数据库名字为story
5. 编译没错后,直接运行Application文件即可,访问地址为:localhost即可(默认端口为80)
6. 访问后台“/admin”或者“/admin/login”,用户名:admin 密码:123456
7. 七牛相关配置：
   - 进入后台设置页面，添加ACCESSKEY和SECRETKEY
   - DOMAIN：为七牛的默认外链域名，注意后面应该有斜杠
   - BUCKET：是七牛存储的空间名字
   - 文章图片地址：因为七牛没有文件夹系统，但可以分层，这个地址就是除了domain，后面自己定义的名字，也该有斜杠
   - 其它图片地址：暂时可以不填 
